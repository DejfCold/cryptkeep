import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, Subject, of } from 'rxjs';
import { ApiService } from '../services/api-service/api.service';
import { filter, tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private api: ApiService,
              private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log('AuthGuard canActivate');
      let isAuth: Observable<boolean | UrlTree> = this.api.isAuthenticated();
      isAuth = isAuth.pipe(catchError(() => of(this.router.createUrlTree(['login']))));
      isAuth = isAuth.pipe(tap(x => console.log('isAuthenticated: ', x)));
      return isAuth;
  }
}
