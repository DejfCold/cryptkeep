import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, from, of, iif } from 'rxjs';
import { CryptoService } from '../services/crypto-service/crypto.service';
import { ApiService } from '../services/api-service/api.service';
import { map, catchError, tap, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PasswordGuard implements CanActivate {
  constructor(private api: ApiService,
              private cryptoService: CryptoService,
              private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log('PasswordGuard canActivate', next);
    if (this.cryptoService.isPassword()) {
      console.log('-- password is set');
      return this.checkPassword()
        .pipe(mergeMap(x => iif(() => x, of(true), of(this.router.createUrlTree(['decrypt'])))))
        .pipe(catchError((e, c) => {
          console.log(e);
          return of(this.router.createUrlTree(['decrypt']));
        }))
        .pipe(tap(x => console.log('-- checkPassword: ', x)));
    } else {
      console.log('-- password is not set');
      return of(this.router.createUrlTree(['']));
    }
  }

  private checkPassword() {
    console.log('---- checking password');
    const decripted = (x: string) => x ? from(this.cryptoService.decrypt(x))
      .pipe(catchError((e, o) => {
        console.log('---- checkPasswordError', e);
        return of(false);
      }))
      .pipe(tap(y => console.log('---- decripted is: ', y)))
      .pipe(map(dec => typeof (dec) === 'string' && (dec as string).length > 0))
      : of(false);

    return this.api.getValidationText()
      .pipe(tap(x => console.log('---- validationText is: ', x)))
      .pipe(tap(x => console.log('---- validationText is not null: ', x != null)))
      .pipe(tap(x => {if (x) {this.cryptoService.setCurrentKeyNonce(x.keyNonce); }}))
      .pipe(map(x => x ? x.validationText : null))
      .pipe(mergeMap(x => iif(() => null != x, decripted(x), of(true))))
      .pipe(catchError((e, o) => {
        console.log('---- validationTextError', e);
        return of(false);
      }))
      .pipe(tap(x => console.log('---- validationTextAfterCatch is: ', x)));
  }



}
