import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {

  constructor() { }
  @Output() passwordSet = new EventEmitter<string>();

  pwdForm: FormGroup = new FormGroup ({
    password: new FormControl('', [Validators.required, Validators.minLength(8)])
  });

  private show = false;

  ngOnInit() {
  }

  ifPwdNotEmpty(x: string) {
    return this.pwdForm.controls.password.value == null || this.pwdForm.controls.password.value === '' ? ' ' : x;
  }

  setDecryptionPwd() {
    console.log(this.pwdForm);
    const pwd =  this.pwdForm.controls.password.value;
    if (pwd == null || pwd === '') {
      return;
    }
    this.passwordSet.emit(this.pwdForm.controls.password.value);
  }

  canContinue() {
    const pwd = this.pwdForm.get('password');
    return pwd.valid && pwd.touched;
  }
  getPwdAttr() {
    if (this.show) {
      return 'text';
    } else {
      return 'password';
    }
  }
  getToggleText() {
    if (this.show) {
      return 'hide';
    } else {
      return 'show';
    }
  }
  toggleShow() {
    this.show = !this.show;
  }
}
