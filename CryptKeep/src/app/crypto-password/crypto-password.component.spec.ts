import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CryptoPasswordComponent } from './crypto-password.component';

describe('CryptoPasswordComponent', () => {
  let component: CryptoPasswordComponent;
  let fixture: ComponentFixture<CryptoPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CryptoPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptoPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
