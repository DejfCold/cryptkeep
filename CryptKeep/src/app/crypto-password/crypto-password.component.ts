import { Component, OnInit } from '@angular/core';
import { CryptoService } from '../services/crypto-service/crypto.service';
import { Router } from '@angular/router';
import { ApiService } from '../services/api-service/api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-crypto-password',
  templateUrl: './crypto-password.component.html',
  styleUrls: ['./crypto-password.component.scss']
})
export class CryptoPasswordComponent implements OnInit {
  isNewUser: Observable<boolean>;
  constructor(private api: ApiService,
              private cryptoService: CryptoService,
              private router: Router) { }

  ngOnInit() {
    this.api.getUserId().subscribe(x => this.cryptoService.setSalt(x));
    this.isNewUser = this.api.isNewUser();
  }
  setDecryptionPwd(event: string) {
    this.cryptoService.setPassword(event);
    this.router.navigate(['notes']);
  }
}
