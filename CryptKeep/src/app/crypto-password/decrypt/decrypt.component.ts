import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CryptoService } from 'src/app/services/crypto-service/crypto.service';

@Component({
  selector: 'app-decrypt',
  templateUrl: './decrypt.component.html',
  styleUrls: ['./decrypt.component.scss']
})
export class DecryptComponent implements OnInit {
  @Output() passwordSet = new EventEmitter<string>();

  pwdForm: FormGroup = new FormGroup ({
    password: new FormControl('', [Validators.required])
  });

  constructor() { }

  ngOnInit() {
  }

  ifPwdNotEmpty(x: string) {
    return this.pwdForm.controls.password.value == null || this.pwdForm.controls.password.value === '' ? '' : x;
  }

  canContinue() {
    const pwd = this.pwdForm.get('password');
    return !pwd.invalid;
  }
  setDecryptionPwd() {
    console.log(this.pwdForm);
    const pwd =  this.pwdForm.controls.password.value;
    if (pwd == null || pwd === '') {
      return;
    }
    this.passwordSet.emit(this.pwdForm.controls.password.value);
  }

}
