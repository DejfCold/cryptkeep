export const Assets = {
    NOTES: '/assets/notes-white-18dp.svg',
    LIST: '/assets/list-white-18dp.svg',
    CHECK_BOX: '/assets/check_box-white-18dp.svg',
    CHACK_BOX_OUTLINE_BLANK: '/assets/check_box_outline_blank-white-18dp.svg'
};
