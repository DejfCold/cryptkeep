import { Injectable } from '@angular/core';
import { ApiService } from '../api-service/api.service';
import { Note } from '../../dto/note';
import { Observable, Subject, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class NoteService {
  private notes: Note[] = [];
  private noteEmmiter = new Subject<Note[]>();
  private eventEmmiter = new Subject<Events>();

  constructor(private api: ApiService) { }


  public observe(): Observable<Note[]> {
    return this.noteEmmiter;
  }

  public observeEvents(): Observable<Events> {
    return this.eventEmmiter;
  }

  public closeCurrent() {
    this.eventEmmiter.next(Events.CLOSE_CURRENT);
  }
  public saveCurrent() {
    this.eventEmmiter.next(Events.SAVE_CURRENT)
  }


  private sendToSubjects(notes: Note[]) {
    this.noteEmmiter.next(notes);
  }
  getNotes(): Observable<Note[]> {
    return this.processNoteObservable(this.api.getNotes());
  }

  addNote(note: Note): Observable<Note[]> {
    if (note.name.length === 0 && (note.noteContents.length === 0 || note.noteContents[0].content.length === 0)) {
      console.warn('Empty note. Won\'t save');
      return of();
    }

    this.generateIdIfNotExists(note);

    const noteCopy: Note = this.copy(note);
    this.notes.push(noteCopy);
    return this.processNoteObservable(this.api.addNote(this.notes));
  }

  editNote(note: Note): Observable<Note[]> {
    const replaceNoteIndex = this.notes.findIndex(x => x.id === note.id);
    this.generateIdIfNotExists(note);

    const noteCopy: Note = this.copy(note);
    this.notes[replaceNoteIndex] = noteCopy;
    return this.processNoteObservable(this.api.editNote(this.notes));
  }

  deleteNote(noteId: string): Observable<Note[]> {
    const deleteNoteIndex = this.notes.findIndex(x => x.id === noteId);
    if (deleteNoteIndex === -1) {
      throw new Error('Note <' + noteId + '> does not exist');
    }

    this.notes.splice(deleteNoteIndex, 1);
    return this.processNoteObservable(this.api.editNote(this.notes));
  }

  private processNoteObservable(note: Observable<Note[]>) {
    return note.pipe(tap(x => this.notes = x))
    .pipe(tap(() => console.log('Loaded', this.notes)))
    .pipe(tap(x => this.sendToSubjects(x)));
  }

  private generateIdIfNotExists(note: Note) {
    if (!note.id) { note.id = this.generateId(); }
    note.noteContents.forEach(x => {if (!x.id) {x.id = this.generateId(); }});
  }

  private generateId(): string {
    return uuid();
  }

  private copy<T>(obj: T): T {
    return JSON.parse(JSON.stringify(obj));
  }
}
export enum Events {
  CLOSE_CURRENT,
  SAVE_CURRENT
}