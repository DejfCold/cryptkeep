import { Injectable } from '@angular/core';
import { NoteDto } from '../../dto/noteDto';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of, from, Subject, Observer } from 'rxjs';
import { map, tap, filter } from 'rxjs/operators';
import { Note } from '../../dto/note';
import { CryptoService } from '../crypto-service/crypto.service';
import { environment } from 'src/environments/environment';
import { ValidationTextDto } from '../../dto/ValidationTextDto';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient, private cryptoService: CryptoService) {
    this.cryptoService.setCurrentKeyNonce(0);
    this.cryptoService.setSalt('salt');
  }

  // testData: Observable<NoteDto> = of({
  //   keyNonce: 4,
  //   id: null,
  //   validationText: null,
  //   // tslint:disable-next-line: max-line-length
  //   note: `sgGe7yx3ahMFjLtUpSap8We9I5X++32SzuY6jzPxD542uTqMkyiLXtP50Zu0JKByzSa13NQFo0j9j296Y35rAyp3HXPMu3tgxrRpsuL5pCUgKm5jSSzcBjyA5NSB1Nm+IQv3JwidPa5kJNEsqoB+dO1M1MOBj798ZtlGdH+2f2dflCf81KgnAp8FSIBDE6cKFkZzI7qCG2dNuXjS8Jzs9Uo2Cmshz5gYfXJpx3XFnMGiR/mCJfbvn7VoiXvTfgUzTC8xVABVz3fnpM1WUzBqnNOFYhwID5gi5vRBMu1ghdk0dAm60wj2Jmla3xffieid+R1ujxM+2Fpzi+rx3l1RW23+XPeD3lrWMnAePLbiKZPxY0WZkfgsziESKBxrxzl03cWI97cUr1YmbedXpoT4ZVc2aDpYubqM5ubA9bleXRZyfUBDu9s4Rx2+LU/4+RDNUNe5vJFudMrzRWSFtri274nChUF2B+dSg3eAcHxMLNDAxqkZVijPFTgfC70LBh7BX2D+oStnaCYGP9mXUrlBIRb5jl7oO3IEVlUms3LO7VRpBxYraZQAkWQynetmNYkrAtXMBIDm4UkearB/wSrsGpAc47lXHUn3ciGR2rB9/5zu1tQiOGYHxE+c/A==`});

  private id: string;
  private readonly SERVER: string = environment.server;

  private setId(id: string) {
    console.log('%cset ID to: %s', 'color:red;', id);
    this.id = id;
  }

  isNewUser(): Observable<boolean> {
    return this.http.get<boolean>( this.SERVER + '/api/notes/isNewUser');
  }

  getNotes(): Observable<Note[]> {
    const subject: Subject<Note[]> = new Subject();
    console.log('get notes');

    let preParsed = this.http.get<NoteDto>( this.SERVER + '/api/notes')
      .pipe(tap(this.resolveUnauthorized()));

    preParsed = preParsed
      .pipe(tap(x => console.log('getNotes preParsed', x)))
      .pipe(tap(x => this.setId(x.id)))
      .pipe(tap(x => this.cryptoService.setCurrentKeyNonce(x.keyNonce)))
      .pipe(tap(x => console.log('received get:', x)));

    preParsed
      .pipe(filter(x => null != x.note))
      .pipe(tap(x => console.log( this.SERVER + ' preParsed', 'Note to parse: ', x)))
      .pipe(map(x => this.decryptNote(x.note)))
      .subscribe(x => x.subscribe( y => subject.next(y)));

    preParsed
      .pipe(filter(x => null == x.note))
      .subscribe(() => subject.next([]));
    return subject;
  }

  public getValidationText(): Observable<ValidationTextDto> {
    console.log(this.SERVER);
    return this.http.get<ValidationTextDto>( this.SERVER + '/api/notes/validationText');
  }

  private encryptNote(note: string) {
    const subject: Subject<string> = new Subject();
    // this.cryptoService.incrementNonce();
    from(this.cryptoService.encrypt(note))
    .subscribe(x => subject.next(x));
    return subject;
  }

  private decryptNote(note: string) {
    const subject: Subject<Note[]> = new Subject();
    if (null == note) {
      return of([]);
    }
    from(this.cryptoService.decrypt(note))
    .pipe(tap(x => console.log('decryptNote', 'Note to parse: ', x)))
    .pipe(map(x => JSON.parse(x) as Note[]))
    .pipe(tap(x => console.log('decryptNote', 'Parsed note: ', x)))
    .subscribe(x => subject.next(x));
    return subject;
  }

  addNote(note: Note[]): Observable< Note[]> {
    const subject: Subject<Note[]> = new Subject();
    this.cryptoService.incrementNonce();
    this.encryptNote(JSON.stringify(note)).subscribe(encyptedNote => {
      this.createNoteDto(encyptedNote).subscribe(postNote => {
        console.log('sending add:', postNote);
        this.http.post<NoteDto>( this.SERVER + '/api/notes', postNote)
        .pipe(tap(this.resolveUnauthorized()))
        .pipe(tap(x => this.setId(x.id)))
        .pipe(tap(x => this.cryptoService.setCurrentKeyNonce(x.keyNonce)))
        .pipe(tap(x => console.log('received add:', x)))
        .pipe(tap(x => console.log('addNote', 'Note to parse: ', x.note)))
        // .pipe(map(x => JSON.parse(x.note)))
        .pipe(map(x => this.decryptNote(x.note)))
        .subscribe(x => x.subscribe( y => subject.next(y)));
      });
    });
    return subject;
  }

  editNote(note: Note[]): Observable< Note[]> {
    const subject: Subject<Note[]> = new Subject();
    this.cryptoService.incrementNonce();
    this.encryptNote(JSON.stringify(note)).subscribe(encyptedNote => {
      this.createNoteDto(encyptedNote).subscribe(putNote => {
        console.log('sending edit:', putNote);
        this.http.put<NoteDto>( this.SERVER + '/api/notes/' + this.id, putNote)
        .pipe(tap(this.resolveUnauthorized()))
        .pipe(tap(x => this.setId(x.id)))
        .pipe(tap(x => this.cryptoService.setCurrentKeyNonce(x.keyNonce)))
        .pipe(tap(x => console.log('received edit:', note)))
        .pipe(tap(x => console.log('editNote', 'Note to parse: ', x.note)))
        .pipe(map(x => this.decryptNote(x.note)))
        .subscribe(x => x.subscribe( y => subject.next(y)));
      });
    });
    return subject;
  }

  private createNoteDto(note: string): Observable<NoteDto> {
    const subject: Subject<NoteDto> = new Subject();
    this.cryptoService.encrypt('validationText').then(x => {
      subject.next({
        id: this.id,
        note,
        validationText: x,
        keyNonce: this.cryptoService.getKeyNonce()
      });
    });
    return subject;
  }

  isAuthenticated(doResolveUnauthorized: boolean = true): Observable<boolean> {
    const ret = this.http.get<boolean>( this.SERVER + '/api/auth/checkAuth');
    if (doResolveUnauthorized) {
      ret.pipe(tap(this.resolveUnauthorized()));
    }
    return ret;
  }

  getUserId(): Observable<string> {
    return this.http.get<any>( this.SERVER + '/api/auth/userId')
    .pipe(tap(this.resolveUnauthorized()))
    .pipe(map(x => x.text));
  }

  private resolveUnauthorized(): Observer<any> {
    return {
      next: (n) => {},
      error: (e) => { if (e.status === 401) { window.location.href = window.location.origin + '/oauth2/authorization/dejfcold'; } },
      complete: () => {}
    };
  }
}
