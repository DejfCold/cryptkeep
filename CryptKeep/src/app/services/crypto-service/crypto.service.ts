import { Injectable } from '@angular/core';
import * as Base64 from 'base64-arraybuffer';
import { ApiService } from '../api-service/api.service';
import { Observer, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  constructor() { }

  private static readonly DERIVED_KEY_TYPE: AesKeyGenParams = {
    name: 'AES-GCM',
    length: 256
  };
  private enc = new TextEncoder();
  private dec = new TextDecoder();
  private password: string;
  private iv: number;
  private salt: string;
  public setSalt(salt: string) {
    this.salt = salt;
  }

  public setPassword(password: string) {
    console.log('setting password to: ', password);
    this.password = password;
  }

  public isPassword() {
    console.log('password is: ', this.password);
    return this.password != null && this.password !== '';
  }

  public setCurrentKeyNonce(nonce: number) {
    this.iv = nonce;
  }
  public incrementNonce() {
    this.iv++;
  }
  public getKeyNonce() {
    return this.iv;
  }

  public async encrypt(message: any): Promise<string> {
    console.log('[encrypt]', this.password, this.iv, this.salt);

    const baseKey = await this.createKeyMaterial(this.password);
    const derivedKey = await this.deriveKey(baseKey, this.getSalt());
    return await this.encryptMessage(derivedKey, message);
  }

  public async decrypt(encryptedMessage: string): Promise<any> {
    console.log('[decrypt]', this.password, this.iv, this.salt);

    const baseKey = await this.createKeyMaterial(this.password);
    const derivedKey = await this.deriveKey(baseKey, this.getSalt());
    return await this.decryptMessage(derivedKey, encryptedMessage);
  }

  private async createKeyMaterial(password: string): Promise<CryptoKey> {
    const keyData = this.enc.encode(password);
    const keyMat = await window.crypto.subtle.importKey(
      'raw',
      keyData,
      'PBKDF2',
      false,
      ['deriveBits', 'deriveKey']
    );
    return keyMat;
  }

  private async deriveKey(baseKey: CryptoKey, salt: Uint8Array): Promise<CryptoKey> {
    const params = this.getPbkdf2Params(salt);

    const derivedKey = await window.crypto.subtle.deriveKey(
      params,
      baseKey,
      CryptoService.DERIVED_KEY_TYPE,
      true,
      ['encrypt', 'decrypt']
    );
    return derivedKey;
  }

  private getPbkdf2Params(salt: Uint8Array) {
    return {
      name: 'PBKDF2',
      salt,
      iterations: 100000,
      hash: {name: 'SHA-256'}
    };
  }

  private getSalt(): Uint8Array {
    const enc = new TextEncoder();
    return enc.encode(this.salt);
  }

  private async encryptMessage(baseKey: CryptoKey, message: any) {
    const algorithm = this.getAlgo();
    const cypherText = await crypto.subtle.encrypt(
      algorithm,
      baseKey,
      this.enc.encode(message)
    );
    return Base64.encode(cypherText);
  }

  private async decryptMessage(baseKey: CryptoKey, encryptedMessage: string) {
    const buffer = Base64.decode(encryptedMessage);
    const plaintext = await crypto.subtle.decrypt(
      this.getAlgo(),
      baseKey,
      buffer
    );
    return this.dec.decode(plaintext);
  }

  private getAlgo() {
    return {
      name: 'AES-GCM',
      iv: this.getNonce()
    };
  }

  private getNonce(): Uint8Array {
    return this.enc.encode(this.iv.toString());
  }
}
