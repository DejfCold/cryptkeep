import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NoteContent } from '../dto/noteContent';
import { Subject } from 'rxjs';
import { NoteService } from '../services/note-service/note.service';

@Component({
  selector: 'app-note-detail',
  templateUrl: './note-detail.component.html',
  styleUrls: ['./note-detail.component.scss']
})
export class NoteDetailComponent implements OnInit {
  @Input() note: NoteContent;
  @Output() detailClose: EventEmitter<NoteContent> = new EventEmitter();
  requestData = new Subject<boolean>();

  ngOnInit() {
    console.log('opened detail', this.note);
  }
  getNote() {
    return this.note;
  }
  closeDetail(note: NoteContent) {
    console.log('closing detail', note);
    note.id = this.note.id;
    // this.detailClose.emit(note);
  }
  closeNote() {
    this.requestData.next(true);
  }

}
