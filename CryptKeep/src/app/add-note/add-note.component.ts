import { Component, OnInit, EventEmitter, Output, Input, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, AbstractControl, FormControl } from '@angular/forms';
import {NoteContent} from '../dto/noteContent';
import { Note } from '../dto/note';
import { FormProvider } from '../providers/form-provider';
import { NoteContentProvider } from '../providers/note-content-provider';
import { NoteProvider } from '../providers/note-provider';
import { Subject } from 'rxjs';
import { NoteService, Events } from '../services/note-service/note.service';
import { Assets } from '../asset-list';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {
  // @Output() newNote = new EventEmitter();
  @Input() note: Note;
  @Input() focus: boolean;
  @Input() sendData: Subject<boolean>;
  @Input() createsNew = false;
  @ViewChild('form') formElement: ElementRef;
  noteForm: FormGroup;
  active = false;
  randomIdFix = Math.floor(Math.random() * 10000) as unknown as string;

  get titlePlaceHolder(): string {
    return this.active ? 'Title ...' : 'Take a note ...';
  }

  constructor(private formBuilder: FormBuilder, private noteService: NoteService) { }

  ngOnInit() {
    console.log('add note', this.note, this.focus);
    this.noteForm = new FormProvider(this.formBuilder).provide(this.note);
    this.subscribeFormEvents(this.noteForm);
    if (this.note) {
      this.addNoteContent();
      this.setActive();
    } else {
      this.createsNew = true;
    }

    if (this.focus) {
      setTimeout(() => {this.formElement.nativeElement.focus(); }, 0);
    }

    this.noteService.observeEvents().subscribe(event => {
      switch(event) {
        case Events.SAVE_CURRENT:
          this.closeNote();
          break;
      }
    })


    // if (this.sendData) {
    //   this.sendData.subscribe(x => {
    //     console.log('sending data');
    //     this.closeNote();
    //   });
    // }
  }
  // listSelectorPicture = this.noteForm.get('isList').value ? Assets.NOTES : Assets.LIST;
  // listSelectorPicture() {
  //   if (this.noteForm.get('isList').value) {
  //     return Assets.NOTES;
  //   } else {
  //     return Assets.LIST;
  //   }
  // }
  // isCheckedPicture(noteContents: FormGroup) {
  //   if (noteContents.get('isChecked').value) {
  //     return Assets.CHECK_BOX;
  //   } else {
  //     return Assets.CHACK_BOX_OUTLINE_BLANK;
  //   }
  // }

  subscribeFormEvents(form: FormGroup) {
    form.controls.noteContents.valueChanges.subscribe((x) => this.modifyEndOfList(x, this));
    form.controls.noteName.valueChanges.subscribe(() => this.createFirstListItem(this));
    form.controls.isList.valueChanges.subscribe((isList) => { this.transform(isList); this.createFirstListItem(this)});
    form.controls.isList.valueChanges.subscribe(() => console.warn('isList valueChanged'));
  }

  createFirstListItem(self: any) {
    if ((self.noteForm.controls.noteContents as FormArray).controls.length === 0) {
      self.addNoteContent();
    }
  }

  /**
   * removes every empty list item at the end of a note except the last one
   * @param val AbstractControl of list items in a note
   * @param self reference to self
   */
  modifyEndOfList(val: any, self: any) {
    const arrSize = val.length;
    if (arrSize >= 2 && val[arrSize - 2].noteContent.length === 0) {
      self.remove(arrSize - 1);
    }
    if (arrSize >= 1 && val[arrSize - 1].noteContent.length >= 1) {
      self.addNoteContent();
    }
  }

  /**
   * removes a list item at specified index
   * @param content index to remove at
   */
  remove(content: number) {
    (this.noteForm.controls.noteContents as FormArray).removeAt(content);
  }

  /**
   * adds unchecked empty list item to a note
   */
  addNoteContent() {
    const newContent = this.formBuilder.group({
      noteContent: [''],
      isChecked: [false]
    });

    (this.noteForm.controls.noteContents as FormArray).push(newContent);
  }

  get noteContentsControls() {
    return (this.noteForm.controls.noteContents as FormArray).controls;
  }

  /**
   * Transforms list to text or vice-versa
   * All list items will be unchecked
   * @param newIsListValue new value of isList
   */
  transform(newIsListValue: boolean) {
    if(newIsListValue) {
      this.transformToList();
    } else {
      this.transformToText();
    }
  }

  transformToList() {
    const noteTextValue = this.noteForm.controls.noteText.value as string;
    const listItemsFromText = noteTextValue.trim().split(`\n`).map(x =>
      this.formBuilder.group({
        noteContent: [x],
        isChecked: [false]
      }));
      const farray = new FormArray([]);
      for(const value of listItemsFromText) {
        farray.push(value);
      }
      farray.push(this.formBuilder.group({
        noteContent: [''],
        isChecked: [false]
      }));
      (this.noteForm.controls.noteContents as FormArray) = farray;

    // const formArray = (this.noteForm.get('noteContents') as FormArray);
    // console.log('before clear', formArray);
    // formArray.clear()
    // console.log('after clear', formArray);
    // for(const value of listItemsFromText) {
    //   formArray.push(value);
    // }
    // console.log('after fill', formArray);
  }

  transformToText() {
    const noteListValues = (this.noteForm.controls.noteContents as FormArray).value as any[];
    const listValuesAsText = noteListValues
      .map(listValue => listValue.noteContent)
      .join('\n');
    (this.noteForm.controls.noteText as FormControl).setValue(listValuesAsText);
  }

  closeNote() {
    const note: Note = new NoteProvider().provide(this.noteForm);
    const noteContents: NoteContent[] = new NoteContentProvider().provide(this.noteForm.value);
    note.noteContents = noteContents;
    console.log('closeNote', note);
    if (this.note) {
      note.id = this.note.id;
    } else {
      note.id = null;
    }

    console.log('emiting', note);

    if (this.createsNew) {
      this.noteService.addNote(note).subscribe();
    } else {
      this.noteService.editNote(note).subscribe();
    }
    // this.newNote.emit(note);
    this.active = false;
    this.ngOnInit();
  }

  setActive() {
    this.active = true;
  }


  // getTitlePlaceholder() {
  //   if (!this.active) {
  //     return 'Take a note ...';
  //   } else {
  //     return 'Title ...';
  //   }
  // }

  deleteNote() {
    this.noteService.deleteNote(this.note.id);
  }

  // blurClass() {
  //   return {visible: this.active};
  // }

  getRandomId(prefix: string, index: number = null): string {
    let randomId = prefix;
    if (index) {
      randomId += '_' + index;
    }
    return randomId + '_' + this.randomIdFix;
  }

  get showNoteText(): boolean {
    return !this.noteForm.controls.isList.value && this.active;
  }
}
