import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NotesViewComponent } from './notes-view/notes-view.component';
import { CryptoPasswordComponent } from './crypto-password/crypto-password.component';
import { AuthGuard } from './guards/auth.guard';
import { PasswordGuard } from './guards/password.guard';
import { LandingComponent } from './landing/landing.component';


const routes: Routes = [
  {path: '', pathMatch: 'full', component: LandingComponent},
  {path: 'decrypt', component: CryptoPasswordComponent, canActivate: [AuthGuard]},
  {path: 'notes', component: NotesViewComponent, canActivate: [AuthGuard, PasswordGuard]},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
