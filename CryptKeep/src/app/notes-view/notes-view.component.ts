import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { NoteContent } from '../dto/noteContent';
import { Note } from '../dto/note';
import { NoteService } from '../services/note-service/note.service';
import { Observable, of, Subject } from 'rxjs';

@Component({
  selector: 'app-notes-view',
  templateUrl: './notes-view.component.html',
  styleUrls: ['./notes-view.component.scss']
})
export class NotesViewComponent implements OnInit {

  constructor(private noteService: NoteService) { }
  @ViewChild('columns') columns;
  noteDetail: NoteContent;
  notes: Observable<Note[]> = this.noteService.observe();
  addingNote = false;

  ngOnInit() {
    this.noteService.getNotes().subscribe(() => this.addingNote = false);
    this.notes.subscribe();
  }

  setAddingNote(addingNote:boolean, event: Event) {
    this.addingNote = addingNote;
    if(!addingNote) {
      console.log('closing current');
      this.noteService.saveCurrent();
    }
    event.stopPropagation();
  }

  openDetail(noteDetail: NoteContent) {
    console.log('openDetail', noteDetail);
    this.noteDetail = noteDetail;
  }

  closeDetail() {
    this.noteService.saveCurrent();
    this.noteDetail = null;
  }
  stopPropagation(event: Event) {
    console.log('stopPropagation', event);
    event.stopPropagation();
  }

}
