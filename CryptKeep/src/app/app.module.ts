import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotesViewComponent } from './notes-view/notes-view.component';
import { NoteComponent } from './note/note.component';
import { NoteItemComponent } from './note-item/note-item.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { AddNoteComponent } from './add-note/add-note.component';
import { NoteDetailComponent } from './note-detail/note-detail.component';
import { CryptoPasswordComponent } from './crypto-password/crypto-password.component';
import { NewUserComponent } from './crypto-password/new-user/new-user.component';
import { DecryptComponent } from './crypto-password/decrypt/decrypt.component';
import { LandingComponent } from './landing/landing.component';

@NgModule({
  declarations: [
    AppComponent,
    NotesViewComponent,
    NoteComponent,
    NoteItemComponent,
    LoginComponent,
    AddNoteComponent,
    NoteDetailComponent,
    CryptoPasswordComponent,
    NewUserComponent,
    DecryptComponent,
    LandingComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
