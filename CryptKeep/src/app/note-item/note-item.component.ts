import { Component, OnInit, Input } from '@angular/core';
import { NoteContent } from '../dto/noteContent';

@Component({
  selector: 'app-note-item',
  templateUrl: './note-item.component.html',
  styleUrls: ['./note-item.component.scss']
})
export class NoteItemComponent implements OnInit {
  @Input() noteContent: NoteContent;
  @Input() isList: boolean;
  randomIdFix = Math.floor(Math.random() * 10000) as unknown as string;
  constructor() { }

  ngOnInit() {
  }

  getRandomId(prefix: string, index: number = null): string {
    let randomId = prefix;
    if (index) {
      randomId += '_' + index;
    }
    return randomId + '_' + this.randomIdFix;
  }
}
