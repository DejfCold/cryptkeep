import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { filter, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ApiService } from '../services/api-service/api.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  constructor(private router: Router,
              private api: ApiService) { }

  ngOnInit() {
    const isAuth: Observable<boolean> = this.api.isAuthenticated(false);
    isAuth.pipe(tap(x => console.log('isAuthenticated: ', x)));
    isAuth.pipe(filter(x => x))
        .subscribe(() => {
          this.router.navigate(['decrypt']);
        });
  }

  toDecrypt() {
    this.router.navigate(['decrypt']);
  }

}
