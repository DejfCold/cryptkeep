import { Note } from '../dto/note';

export class NoteProvider {
    public provide(noteForm: any) {
        const note: Note = {
            id: noteForm.value.id,
            isList: noteForm.value.isList,
            name: noteForm.value.noteName,
            noteContents: []
        };
        return note;
    }

}
