import { Note } from '../dto/note';
import { Validators, FormBuilder, FormControl, FormArray, FormGroup } from '@angular/forms';

export class FormProvider {
    constructor(private formBuilder: FormBuilder) { }
    provide(note: Note): FormGroup {
        if (note) {
            return this.createFormFromNote(note);
        } else {
            return this.createEmptyForm();
        }
    }

    private createEmptyForm(): FormGroup {
        console.log('creating new form');
        const newForm = this.formBuilder.group({
            noteName: ['', [Validators.required]],
            isList: [false],
            noteText: [''],
            noteContents: this.formBuilder.array([])
          });
        return newForm;
    }

    private createFormFromNote(note: Note): FormGroup {
        console.log('creating form from note');
        const noteContents = this.formBuilder.array([]);
        if (note.isList) {
          note.noteContents.map(x => this.formBuilder.group({
            noteContent: [x.content],
            isChecked: [x.isChecked]
          })).forEach(x => noteContents.push(x));
        }
        const newForm = this.formBuilder.group({
          noteName: [note.name, [Validators.required]],
          isList: [note.isList],
          noteText: [note.isList ? '' : note.noteContents[0].content],
          noteContents
        });
        return newForm;
    }
}
