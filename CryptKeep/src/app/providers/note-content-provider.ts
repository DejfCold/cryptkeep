import { NoteContent } from '../dto/noteContent';


export class NoteContentProvider {
    public provide(noteForm: any): NoteContent[] {
        if (noteForm.isList) {
            return this.provideList(noteForm);
        } else {
            return this.provideContent(noteForm);
        }
    }

    private provideContent(noteForm: any): NoteContent[] {
        const noteContents: NoteContent[] = [];
        const noteContent: NoteContent = {
            id: noteForm.noteContents.id || null,
            content: noteForm.noteText,
            isChecked: false,
            order: 0
        };
        noteContents.push(noteContent);
        return noteContents;
    }

    private provideList(noteForm: any): NoteContent[] {
        const noteContents: NoteContent[] = [];
        for (let i = 0; i < noteForm.noteContents.length - 1; i++) {
            const noteContent: NoteContent = {
                id: noteForm.noteContents[i].id || null,
                content: noteForm.noteContents[i].noteContent,
                isChecked: noteForm.noteContents[i].isChecked,
                order: i
            };
            noteContents.push(noteContent);
        }
        return noteContents;
    }
}
