import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from 'protractor';
import { NoteContent } from '../dto/noteContent';
import { Note } from '../dto/note';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {
  @Input() note: Note;
  noteContents: NoteContent[];
  isMaxContent = false;
  constructor() { }

  ngOnInit() {
    this.noteContents = this.getNoteContents().slice(0, 4);
    this.isMaxContent = this.noteContents.length === 4;
  }
  getNoteContents() {
    return this.note.noteContents.sort(this.sortNoteContent);
  }

  private sortNoteContent(a: any, b: any): number {
    return a.order < b.order ? -1 : a.order === b.order ? 0 : 1;
  }

}
