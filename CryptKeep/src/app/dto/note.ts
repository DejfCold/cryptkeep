import { NoteContent as NoteContent } from './noteContent';

export interface Note {
    id: string;
    isList: boolean;
    name: string;
    noteContents: NoteContent[];
}
