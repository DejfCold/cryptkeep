export interface NoteDto {
    id: string;
    note: string;
    keyNonce: number;
    validationText: string;
}
