import { NoteContent as NoteContent } from './noteContent';

export interface ValidationTextDto {
    keyNonce: number;
    validationText: string;
}
