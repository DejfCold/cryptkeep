export interface NoteContent {
    id: string;
    content: string;
    isChecked: boolean;
    order: number;
}
