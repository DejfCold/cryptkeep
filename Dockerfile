FROM openjdk:8-alpine
RUN addgroup -S cryptkeep && adduser -S cryptkeep -G cryptkeep
USER cryptkeep:cryptkeep
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} CryptKeep.jar
ENTRYPOINT ["java","-jar","/CryptKeep.jar"]
