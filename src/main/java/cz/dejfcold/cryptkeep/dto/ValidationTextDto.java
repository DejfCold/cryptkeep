package cz.dejfcold.cryptkeep.dto;

import cz.dejfcold.cryptkeep.jpa.NotePojo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data 
@NoArgsConstructor 
@AllArgsConstructor 
@Builder 
@ToString
public class ValidationTextDto {

	private Integer keyNonce;
	private String validationText;
	
	public static ValidationTextDto noteToValidationTextDto(NotePojo note) {
		return ValidationTextDto.builder()
				.keyNonce(note.getKeyNonce())
				.validationText(note.getValidationText())
				.build();
	}
}
