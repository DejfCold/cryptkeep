package cz.dejfcold.cryptkeep.dto;

import java.util.UUID;
import cz.dejfcold.cryptkeep.jpa.NotePojo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data 
@NoArgsConstructor 
@AllArgsConstructor 
@Builder 
@ToString
public class NoteDto {
	private UUID id;
	private String note;
	private Integer keyNonce;
	private String validationText;
	
	public static NoteDto jpaToDto(NotePojo note) {
		return NoteDto.builder()
				.id(note.getId())
				.note(note.getNote())
				.keyNonce(note.getKeyNonce())
				.validationText(note.getValidationText())
				.build();
	}
	
	public static NotePojo dtoToJpa(NoteDto noteDto) {
		NotePojo note = new NotePojo();
		note.setId(noteDto.getId());
		note.setNote(noteDto.getNote());
		note.setKeyNonce(noteDto.getKeyNonce());
		note.setValidationText(noteDto.getValidationText());
		return note;
	}
}
