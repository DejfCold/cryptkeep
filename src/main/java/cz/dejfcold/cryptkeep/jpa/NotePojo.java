package cz.dejfcold.cryptkeep.jpa;

import javax.persistence.*;

import lombok.Data;

import java.util.UUID;

@Data
@Entity(name="notes")
//@NamedQuery(name="Note.findAll", query="SELECT n FROM Note n")
public class NotePojo {

	@Id
	@GeneratedValue
	private UUID id;
	private String username;
	private String note;
	private Integer keyNonce;
	private String validationText;

}