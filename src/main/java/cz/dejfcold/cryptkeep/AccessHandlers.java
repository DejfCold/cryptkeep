package cz.dejfcold.cryptkeep;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AccessHandlers {
	public static void apiAccessDenied(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException accessDeniedException) throws IOException, ServletException {
		log.info("UNAUTHORIZED");
		log.info(accessDeniedException.getMessage());
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
	}
}
