package cz.dejfcold.cryptkeep.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter @Getter @ToString @EqualsAndHashCode
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "dejfcold.security")
public class SecurityConfig {
	private String[] allowedRoles;
	private String[] adminRoles;
}
