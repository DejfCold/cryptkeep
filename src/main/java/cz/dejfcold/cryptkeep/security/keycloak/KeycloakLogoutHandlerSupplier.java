package cz.dejfcold.cryptkeep.security.keycloak;

import java.util.function.Supplier;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class KeycloakLogoutHandlerSupplier implements Supplier<KeycloakLogoutHandler> {

	@Override
	public KeycloakLogoutHandler get() {
		return new KeycloakLogoutHandler(new RestTemplate());
	}

}
