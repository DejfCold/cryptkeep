package cz.dejfcold.cryptkeep.security.keycloak;

import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.stereotype.Component;

@Component
public class KeycloakOauth2UserServiceSupplier implements Supplier<KeycloakOauth2UserService> {
	private final OAuth2ClientProperties oauth2ClientProperties;
	@Autowired
	public KeycloakOauth2UserServiceSupplier(OAuth2ClientProperties oauth2ClientProperties) {
		this.oauth2ClientProperties = oauth2ClientProperties;
	}

	@Override
	public KeycloakOauth2UserService get() {
		JwtDecoder jwtDecoder = JwtDecoders.fromOidcIssuerLocation(oauth2ClientProperties.getProvider().get("dejfcold").getIssuerUri());
		SimpleAuthorityMapper authoritiesMapper = new SimpleAuthorityMapper();
		authoritiesMapper.setConvertToUpperCase(true);

		return new KeycloakOauth2UserService(jwtDecoder, authoritiesMapper);
	}

}
