package cz.dejfcold.cryptkeep.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.web.OAuth2LoginAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import cz.dejfcold.cryptkeep.AccessHandlers;
import cz.dejfcold.cryptkeep.security.keycloak.KeycloakLogoutHandlerSupplier;
import cz.dejfcold.cryptkeep.security.keycloak.KeycloakOauth2UserServiceSupplier;

@Configuration
public class SecurityAdapter extends WebSecurityConfigurerAdapter {
	private SecurityConfig secuirtyConfig;
	private KeycloakOauth2UserServiceSupplier keycloakOidcUserServiceSupplier;
	private KeycloakLogoutHandlerSupplier keycloakLogoutHandlerSupplier;
	
	@Autowired
	public SecurityAdapter(SecurityConfig securityConfig,
			KeycloakOauth2UserServiceSupplier keycloakOidcUserServiceSupplier,
			KeycloakLogoutHandlerSupplier keycloakLogoutHandlerSupplier) {
		
		this.secuirtyConfig = securityConfig;
		this.keycloakOidcUserServiceSupplier = keycloakOidcUserServiceSupplier;
		this.keycloakLogoutHandlerSupplier = keycloakLogoutHandlerSupplier;
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests(a -> 
			a.antMatchers("/api/**").hasAnyRole(secuirtyConfig.getAllowedRoles())
			.antMatchers("/actuator/**").hasAnyRole(secuirtyConfig.getAdminRoles())
			.antMatchers("/**").permitAll()
			.anyRequest().permitAll()
		)
		.exceptionHandling(e ->
			e.defaultAuthenticationEntryPointFor(AccessHandlers::apiAccessDenied, new AntPathRequestMatcher("/api/**"))
		)
		.csrf(c -> 
			c.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
		)
		.oauth2Login(o ->
			o.userInfoEndpoint(u -> 
				u.oidcUserService(keycloakOidcUserServiceSupplier.get()))
		)
		.logout().addLogoutHandler(keycloakLogoutHandlerSupplier.get());
		
	}
}
