package cz.dejfcold.cryptkeep.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.dejfcold.cryptkeep.jpa.NotePojo;

public interface NoteRepository extends JpaRepository<NotePojo, UUID> {
	List<NotePojo> findAllByUsername(String username);
	NotePojo findByIdAndUsername(UUID id, String username);
}
