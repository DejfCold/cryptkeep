package cz.dejfcold.cryptkeep.api;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/{path:[^\\.]*}")
@Controller
public class SpaForward {
	
	@GetMapping
	public String get(Authentication authentication) {
		return "forward:/";
	}
}
