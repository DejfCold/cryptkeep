package cz.dejfcold.cryptkeep.api;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.dejfcold.cryptkeep.dto.NoteDto;
import cz.dejfcold.cryptkeep.dto.ValidationTextDto;
import cz.dejfcold.cryptkeep.jpa.NotePojo;
import cz.dejfcold.cryptkeep.repository.NoteRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@RequestMapping("/api/notes")
@RestController
@Slf4j
public class Notes {
	private NoteRepository noteRepository;
	
	@Autowired
	public Notes(NoteRepository noteRepository) {
		this.noteRepository = noteRepository;
	}
	
	@GetMapping("/validationText")
	public Optional<ValidationTextDto> getValidationText(@NonNull Authentication authentication) {
		Optional<NotePojo> note = noteRepository.findAllByUsername(authentication.getName()).stream()
				.findFirst();
		if(note.isPresent()) {
			return Optional.of(ValidationTextDto.noteToValidationTextDto(note.get()));
		} else {
			return Optional.empty();
		}
				
	}
	@GetMapping("/isNewUser")
	public Boolean isNewUser(@NonNull Authentication authentication) {
		return noteRepository.findAllByUsername(authentication.getName()).isEmpty();
	}
	
	@GetMapping
	public NoteDto getNotes(@NonNull Authentication authentication) {
		return noteRepository
				.findAllByUsername(authentication.getName()).stream()
				.map(NoteDto::jpaToDto)
				.findFirst()
				.orElse(NoteDto.builder()
						.id(UUID.randomUUID())
						.keyNonce(0)
						.note(null)
						.validationText(null)
						.build());
	}
	
	@PostMapping 
	public NoteDto addNote(@RequestBody NoteDto noteDto, @NonNull Authentication authentication) {
		log.info(noteDto.toString());
		NotePojo noteToSave = NoteDto.dtoToJpa(noteDto);
		noteToSave.setUsername(authentication.getName());
		NotePojo savedNote = this.noteRepository.saveAndFlush(noteToSave);
		return NoteDto.jpaToDto(savedNote);
	}
	
	@PutMapping("/{noteId}")
	public NoteDto editNote(@RequestBody NoteDto noteDto, @PathVariable("noteId") UUID noteId, @NonNull Authentication authentication) {
		Assert.isTrue(noteId.equals(noteDto.getId()), "NoteDto.Id has to be the same as {noteId}");
		NotePojo noteToUpdate = this.noteRepository.findByIdAndUsername(noteId, authentication.getName());
		noteToUpdate.setNote(noteDto.getNote());
		noteToUpdate.setKeyNonce(noteDto.getKeyNonce());
		noteToUpdate.setValidationText(noteDto.getValidationText());
		NotePojo savedNote = this.noteRepository.save(noteToUpdate);
		return NoteDto.jpaToDto(savedNote);
	}
}
