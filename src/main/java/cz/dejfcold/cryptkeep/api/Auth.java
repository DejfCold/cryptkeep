package cz.dejfcold.cryptkeep.api;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.dejfcold.cryptkeep.dto.TextDto;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@RequestMapping("/api/auth")
@RestController
@Slf4j
public class Auth {

	@GetMapping("/checkAuth")
	public boolean isAuthenticated(@NonNull Authentication authentication) {
		log.info("Authorities: {}\r\n\t Details: {}", authentication.getAuthorities(), authentication.getDetails());
		return authentication.isAuthenticated();
	}
	
	@GetMapping("/userId")
	public TextDto getUserId(@NonNull Authentication authentication) {
		
		return TextDto.builder()
				.text(authentication.getName())
				.build();
	}
	
}
