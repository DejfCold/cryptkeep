-- Table: public.notes

-- DROP TABLE public.notes;

CREATE TABLE public.notes
(
    id uuid NOT NULL,
    username text COLLATE pg_catalog."default" NOT NULL,
    note text COLLATE pg_catalog."default",
    validation_text text COLLATE pg_catalog."default",
    key_nonce numeric NOT NULL DEFAULT 0,
    CONSTRAINT notes_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.notes
    OWNER to postgres;