CREATE DATABASE cryptkeep
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
    
GRANT ALL ON DATABASE "cryptkeep" TO postgres;

GRANT TEMPORARY, CONNECT ON DATABASE "cryptkeep" TO PUBLIC;

GRANT ALL ON DATABASE "cryptkeep" TO cryptkeep;